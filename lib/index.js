module.exports = {
    describeModel: require('./describe-model'),
    truncate: require('./truncate')
};
