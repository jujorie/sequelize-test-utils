'use strict';

((module) => {
    /**
    * Truncate all dabase models
    * @param {object} models - Models to sequelize
    * @param {boolean} [throwError=true] - True if throw Error
    */
    async function truncate(models, throwError = true) {
        let counter = 100;
        const modelKeys = Object
            .keys(models)
            .filter((key) => key.toLowerCase() !== 'sequelize');

        while (modelKeys.length && counter) {
            const modelKey = modelKeys.shift();
            try {
                await models[modelKey].destroy({
                    where: {},
                    force: true,
                });
            } catch (ex) {
                modelKeys.push(modelKey);
            }
            counter--;
        }
        if (!counter && throwError) {
            throw new Error(`Keys remaind ${JSON.stringify(modelKeys)}`);
        }
    }

    module.exports = truncate;
})(module);
