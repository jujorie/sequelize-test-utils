'use strict';

((module) => {
    const truncate = require('./truncate');

    /**
     * Executes a basic test for models
     * @param {Object} models -Models
     * @param {Object} model - Model to test
     * @param {Object} factories - models factories
     * @param {Object} [updateValues=null] - Valid update values
     * @param {Array<{name:string, value:any, message:string}>} [invalidValues]
     * - values to be tested that are invalids
     * @param {fn} [initCallback=null] - callback call on init all
     * @param {fn} [joinTest = null] - suite to test joins
     */
    function describeModel(
        models,
        model,
        factories,
        updateValues = null,
        invalidValues = null,
        initCallback = null,
        joinTest = null
    ) {
        const modelNameRaw = model.name;
        const modelName = modelNameRaw;
        const exclude = ['id', 'createdAt', 'updatedAt'];

        describe(`'${modelName}' Crud operations`, () => {
            beforeAll(async () => {
                await truncate(models);
                if (typeof initCallback === 'function') {
                    await initCallback();
                }
            });

            if (joinTest) {
                describe('Join tests', joinTest);
            }

            describe('Create', () => {
                it(`It must create a '${modelName}'`, async () => {
                    const newItem = await factories[modelName]();
                    expect(newItem).toBeDefined();
                });
            });

            describe('Update', () => {
                let id;

                beforeEach(async () => {
                    const createdItem = await factories[modelName]();
                    expect(createdItem).toBeDefined();
                    id = createdItem.id;
                });

                it(`It must edit a '${modelName}'`, async () => {
                    if (!id) {
                        return;
                    }
                    let savedItem = null;
                    const foundItem = await model.findByPk(id);
                    expect(foundItem).not.toBeNull();

                    if (updateValues) {
                        Object.keys(updateValues)
                            .forEach((key) => {
                                foundItem[key] = updateValues[key];
                            });
                        savedItem = await foundItem.save();
                        Object.keys(updateValues)
                            .forEach((key) => {
                                expect(savedItem[key]).toBe(foundItem[key]);
                            });
                    }
                });
            });

            describe(`Validations`, () => {
                const attrs = model.rawAttributes;

                Object.keys(attrs)
                    .filter((attrName) => {
                        return -1 === exclude.indexOf(attrName)
                            && false === attrs[attrName].allowNull;
                    })
                    .forEach((attrName) => {
                        it(`Must fail if ${attrName} is null`, async () => {
                            try {
                                const props = {};
                                props[attrName] = null;
                                await factories[modelName](props);
                                throw new Error('__invalid');
                            } catch (err) {
                                expect(err.message)
                                    .toBe(
                                        `notNull Violation: ` +
                                        `${modelNameRaw}.${attrName} ` +
                                        `cannot be null`
                                    );
                            }
                        });
                    });

                if (invalidValues && invalidValues.length) {
                    invalidValues.map((invalidValue) => {
                        it(
                            `must fail if '${invalidValue.name}' `
                            + `has value '${invalidValue.value}'`,
                            async () => {
                                try {
                                    const props = {};
                                    props[invalidValue.name]
                                        = invalidValue.value;
                                    await factories[modelName](props);
                                    throw new Error('__invalid');
                                } catch (err) {
                                    expect(err.message)
                                        .toBe(invalidValue.message);
                                }
                            });
                    });
                }
            });
        });
    };

    module.exports = describeModel;
})(module);
